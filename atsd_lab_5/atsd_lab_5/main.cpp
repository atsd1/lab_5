﻿#include <iostream>
#include <chrono>
using namespace std;

template<class T>
class LinkedListNode {
public:
    T value;
    LinkedListNode* next;
    LinkedListNode* prev;

    LinkedListNode<T>(T value) : value(value) {}
};

template<class T>
class LinkedList {
public:
    LinkedListNode<T>* begin;

    LinkedList<T>() : begin(nullptr) {}

    void PushFirst(T value) {
        LinkedListNode<T>* node = new LinkedListNode<T>(value);

        if (begin == nullptr)
        {
            begin = node;
            return;
        }

        begin->prev = node;
        node->next = begin;

        begin = node;
    }

    void PushLast(T value) {
        LinkedListNode<T>* node = new LinkedListNode<T>(value);

        if (begin == nullptr)
        {
            begin = node;
            return;
        }

        LinkedListNode<T>* current = begin;

        while (current->next != nullptr)
        {
            current = current->next;
        }

        current->next = node;
        node->prev = current;
    }

    void PushAt(T value, int index) {
        if (index == 0)
        {
            return PushFirst(value);
        }

        LinkedListNode<T>* node = new LinkedListNode<T>(value);
        LinkedListNode<T>* current = begin;

        for (int i = 0; i < index; i++)
        {
            current = current->next;
        }

        current->prev->next = node;
        node->prev = current->prev;
        node->next = current;
    }

    void DeleteFirst() {
        LinkedListNode<T>* current = begin;

        if (begin->next == nullptr)
        {
            begin = nullptr;
            delete current;
            return;
        }

        begin = begin->next;

        current->next = nullptr;
        begin->prev = nullptr;

        delete current;
    }

    void DeleteLast() {
        LinkedListNode<T>* current = begin;

        if (begin->next == nullptr)
        {
            begin = nullptr;
            delete current;
            return;
        }

        while (current->next != nullptr)
        {
            current = current->next;
        }

        current->prev->next = nullptr;
        current->prev = nullptr;

        delete current;
    }

    void DeleteAt(int index) {
        LinkedListNode<T>* current = begin;
        for (int i = 0; i < index; i++)
        {
            current = current->next;
        }

        current->prev->next = current->next;
        current->next->prev = current->prev;

        delete current;
    }

    void PrintList() {
        LinkedListNode<T>* current = begin;
        while (current != nullptr)
        {
            cout << current->value << endl;
            current = current->next;
        }
    }

    void Clear() {
        while (begin != nullptr)
        {
            DeleteLast();
        }
    }

    int Count() {
        int count = 0;
        LinkedListNode<T>* current = begin;

        while (current != nullptr)
        {
            current = current->next;
            count++;
        }
        return count;
    }

    T At(int index) {
        return NodeAt(index)->value;
    }

    void SelectSort() {
        LinkedListNode<T>* current = begin;
        while (current != nullptr)
        {
            LinkedListNode<T>* min = current;
            LinkedListNode<T>* next = current->next;
            while (next != nullptr)
            {
                if (min->value > next->value)
                {
                    min = next;
                }
                next = next->next;
            }

            T temp = current->value;
            current->value = min->value;
            min->value = temp;

            current = current->next;
        }
    }
    void InsertSort() {
        LinkedListNode<T>* current = begin;

        while (current != nullptr)
        {
            LinkedListNode<T>* currentSwapNode = current;
            LinkedListNode<T>* prev = currentSwapNode->prev;

            while (prev != nullptr && currentSwapNode->value < prev->value)
            {
                T temp = prev->value;
                prev->value = currentSwapNode->value;
                currentSwapNode->value = temp;

                current = prev;
                prev = prev->prev;
            }
            current = current->next;
        }
    }
private:
    LinkedListNode<T>* NodeAt(int index) {
        LinkedListNode<T>* current = begin;

        for (int i = 0; i < index; i++)
        {
            current = current->next;
        }

        return current;
    }
};

void InsertArraySort(int arr[], int lenght)
{
    for (int i = 0; i < lenght; i++)
    {
        int temp = arr[i];
        int j;

        for (j = i - 1; j >= 0; j--)
        {
            if (arr[j] < temp)
            {
                break;
            }

            arr[j + 1] = arr[j];
        }

        arr[j + 1] = temp;
    }
}

void TimeListSelectionSort(LinkedList<int> list) {
    auto t1 = chrono::steady_clock::now();
    auto t2 = chrono::steady_clock::now();

    list.Clear();
    for (int i = 0; i < 10; i++)
    {
        list.PushLast(rand());
    }
    t1 = chrono::steady_clock::now();
    list.SelectSort();
    t2 = chrono::steady_clock::now();
    cout << "list.SelectSort() Size = " << list.Count() << " : " << chrono::duration_cast<chrono::microseconds>((t2 - t1)).count() / 1000.0 << " milliseconds\n";

    list.Clear();
    for (int i = 0; i < 100; i++)
    {
        list.PushLast(rand());
    }
    t1 = chrono::steady_clock::now();
    list.SelectSort();
    t2 = chrono::steady_clock::now();
    cout << "list.SelectSort() Size = " << list.Count() << " : " << chrono::duration_cast<chrono::microseconds>((t2 - t1)).count() / 1000.0 << " milliseconds\n";

    list.Clear();
    for (int i = 0; i < 500; i++)
    {
        list.PushLast(rand());
    }
    t1 = chrono::steady_clock::now();
    list.SelectSort();
    t2 = chrono::steady_clock::now();
    cout << "list.SelectSort() Size = " << list.Count() << " : " << chrono::duration_cast<chrono::microseconds>((t2 - t1)).count() / 1000.0 << " milliseconds\n";

    list.Clear();
    for (int i = 0; i < 1000; i++)
    {
        list.PushLast(rand());
    }
    t1 = chrono::steady_clock::now();
    list.SelectSort();
    t2 = chrono::steady_clock::now();
    cout << "list.SelectSort() Size = " << list.Count() << " : " << chrono::duration_cast<chrono::microseconds>((t2 - t1)).count() / 1000.0 << " milliseconds\n";

    list.Clear();
    for (int i = 0; i < 2000; i++)
    {
        list.PushLast(rand());
    }
    t1 = chrono::steady_clock::now();
    list.SelectSort();
    t2 = chrono::steady_clock::now();
    cout << "list.SelectSort() Size = " << list.Count() << " : " << chrono::duration_cast<chrono::microseconds>((t2 - t1)).count() / 1000.0 << " milliseconds\n";

    list.Clear();
    for (int i = 0; i < 5000; i++)
    {
        list.PushLast(rand());
    }
    t1 = chrono::steady_clock::now();
    list.SelectSort();
    t2 = chrono::steady_clock::now();
    cout << "list.SelectSort() Size = " << list.Count() << " : " << chrono::duration_cast<chrono::microseconds>((t2 - t1)).count() / 1000.0 << " milliseconds\n";

    list.Clear();
    for (int i = 0; i < 10000; i++)
    {
        list.PushLast(rand());
    }
    t1 = chrono::steady_clock::now();
    list.SelectSort();
    t2 = chrono::steady_clock::now();
    cout << "list.SelectSort() Size = " << list.Count() << " : " << chrono::duration_cast<chrono::microseconds>((t2 - t1)).count() / 1000.0 << " milliseconds\n";

}

void TimeListInsertionSort(LinkedList<int> list) {
    auto t1 = chrono::steady_clock::now();
    auto t2 = chrono::steady_clock::now();

    list.Clear();
    for (int i = 0; i < 10; i++)
    {
        list.PushLast(rand());
    }
    t1 = chrono::steady_clock::now();
    list.InsertSort();
    t2 = chrono::steady_clock::now();
    cout << "list.InsertSort() Size = " << list.Count() << " : " << chrono::duration_cast<chrono::microseconds>((t2 - t1)).count() / 1000.0 << " milliseconds\n";

    list.Clear();
    for (int i = 0; i < 100; i++)
    {
        list.PushLast(rand());
    }
    t1 = chrono::steady_clock::now();
    list.InsertSort();
    t2 = chrono::steady_clock::now();
    cout << "list.InsertSort() Size = " << list.Count() << " : " << chrono::duration_cast<chrono::microseconds>((t2 - t1)).count() / 1000.0 << " milliseconds\n";

    list.Clear();
    for (int i = 0; i < 500; i++)
    {
        list.PushLast(rand());
    }
    t1 = chrono::steady_clock::now();
    list.InsertSort();
    t2 = chrono::steady_clock::now();
    cout << "list.InsertSort() Size = " << list.Count() << " : " << chrono::duration_cast<chrono::microseconds>((t2 - t1)).count() / 1000.0 << " milliseconds\n";

    list.Clear();
    for (int i = 0; i < 1000; i++)
    {
        list.PushLast(rand());
    }
    t1 = chrono::steady_clock::now();
    list.InsertSort();
    t2 = chrono::steady_clock::now();
    cout << "list.InsertSort() Size = " << list.Count() << " : " << chrono::duration_cast<chrono::microseconds>((t2 - t1)).count() / 1000.0 << " milliseconds\n";

    list.Clear();
    for (int i = 0; i < 2000; i++)
    {
        list.PushLast(rand());
    }
    t1 = chrono::steady_clock::now();
    list.InsertSort();
    t2 = chrono::steady_clock::now();
    cout << "list.InsertSort() Size = " << list.Count() << " : " << chrono::duration_cast<chrono::microseconds>((t2 - t1)).count() / 1000.0 << " milliseconds\n";

    list.Clear();
    for (int i = 0; i < 5000; i++)
    {
        list.PushLast(rand());
    }
    t1 = chrono::steady_clock::now();
    list.InsertSort();
    t2 = chrono::steady_clock::now();
    cout << "list.InsertSort() Size = " << list.Count() << " : " << chrono::duration_cast<chrono::microseconds>((t2 - t1)).count() / 1000.0 << " milliseconds\n";

    list.Clear();
    for (int i = 0; i < 10000; i++)
    {
        list.PushLast(rand());
    }
    t1 = chrono::steady_clock::now();
    list.InsertSort();
    t2 = chrono::steady_clock::now();
    cout << "list.InsertSort() Size = " << list.Count() << " : " << chrono::duration_cast<chrono::microseconds>((t2 - t1)).count() / 1000.0 << " milliseconds\n";


}

void TimeArrayInsertionSort() {
    auto t1 = chrono::steady_clock::now();
    auto t2 = chrono::steady_clock::now();

    int arr10[10];
    for (int i = 0; i < 10; i++)
    {
        arr10[i] = rand();
    }
    t1 = chrono::steady_clock::now();
    InsertArraySort(arr10, 10);
    t2 = chrono::steady_clock::now();
    cout << "InsertArraySort() Size = " << 10 << " : " << chrono::duration_cast<chrono::microseconds>((t2 - t1)).count() / 1000.0 << " milliseconds\n";

    int arr100[100];
    for (int i = 0; i < 100; i++)
    {
        arr100[i] = rand();
    }
    t1 = chrono::steady_clock::now();
    InsertArraySort(arr100, 100);
    t2 = chrono::steady_clock::now();
    cout << "InsertArraySort() Size = " << 100 << " : " << chrono::duration_cast<chrono::microseconds>((t2 - t1)).count() / 1000.0 << " milliseconds\n";

    int arr500[500];
    for (int i = 0; i < 500; i++)
    {
        arr500[i] = rand();
    }
    t1 = chrono::steady_clock::now();
    InsertArraySort(arr500, 500);
    t2 = chrono::steady_clock::now();
    cout << "InsertArraySort() Size = " << 500 << " : " << chrono::duration_cast<chrono::microseconds>((t2 - t1)).count() / 1000.0 << " milliseconds\n";

    int arr1000[1000];
    for (int i = 0; i < 1000; i++)
    {
        arr1000[i] = rand();
    }
    t1 = chrono::steady_clock::now();
    InsertArraySort(arr1000, 1000);
    t2 = chrono::steady_clock::now();
    cout << "InsertArraySort() Size = " << 1000 << " : " << chrono::duration_cast<chrono::microseconds>((t2 - t1)).count() / 1000.0 << " milliseconds\n";

    int arr2000[2000];
    for (int i = 0; i < 2000; i++)
    {
        arr2000[i] = rand();
    }
    t1 = chrono::steady_clock::now();
    InsertArraySort(arr2000, 2000);
    t2 = chrono::steady_clock::now();
    cout << "InsertArraySort() Size = " << 2000 << " : " << chrono::duration_cast<chrono::microseconds>((t2 - t1)).count() / 1000.0 << " milliseconds\n";

    int arr5000[5000];
    for (int i = 0; i < 5000; i++)
    {
        arr5000[i] = rand();
    }
    t1 = chrono::steady_clock::now();
    InsertArraySort(arr5000, 5000);
    t2 = chrono::steady_clock::now();
    cout << "InsertArraySort() Size = " << 5000 << " : " << chrono::duration_cast<chrono::microseconds>((t2 - t1)).count() / 1000.0 << " milliseconds\n";

    int arr10000[10000];
    for (int i = 0; i < 10000; i++)
    {
        arr10000[i] = rand();
    }
    t1 = chrono::steady_clock::now();
    InsertArraySort(arr10000, 10000);
    t2 = chrono::steady_clock::now();
    cout << "InsertArraySort() Size = " << 10000 << " : " << chrono::duration_cast<chrono::microseconds>((t2 - t1)).count() / 1000.0 << " milliseconds\n";


}

int main() {

    LinkedList<int> list = LinkedList<int>();

    TimeListSelectionSort(list); cout << endl;
    TimeListInsertionSort(list); cout << endl;
    TimeArrayInsertionSort(); cout << endl;
}